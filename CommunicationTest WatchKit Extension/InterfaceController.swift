//
//  InterfaceController.swift
//  CommunicationTest WatchKit Extension
//
//  Created by Parrot on 2019-10-26.
//  Copyright © 2019 Parrot. All rights reserved.
//

import WatchKit
import Foundation
import WatchConnectivity

class InterfaceController: WKInterfaceController, WCSessionDelegate {

    
    // MARK: Outlets
    // ---------------------
    @IBOutlet var messageLabel: WKInterfaceLabel!
   
    // Imageview for the pokemon
    @IBOutlet var pokemonImageView: WKInterfaceImage!
    // Label for Pokemon name (Albert is hungry)
    @IBOutlet var nameLabel: WKInterfaceLabel!
    // Label for other messages (HP:100, Hunger:0)
    @IBOutlet var outputLabel: WKInterfaceLabel!
    
    
    
    var pokemonRecieved = ""
    var pokemonName = ""
    var hungerlevel = 0
    var healthlevel = 100
    var timer = Timer()
    var seconds:Int=0
    
    
    
    // MARK: Delegate functions
    // ---------------------

    // Default function, required by the WCSessionDelegate on the Watch side
    func session(_ session: WCSession, activationDidCompleteWith activationState: WCSessionActivationState, error: Error?) {
        //@TODO
    }
    
    // 3. Get messages from PHONE
    func session(_ session: WCSession, didReceiveMessage message: [String : Any]) {
        print("WATCH: Got message from Phone")
        // Message from phone comes in this format: ["course":"MADT"]
        //let messageBody = message["course"] as! String
        let messageBody = message["pokemon"] as! String
        messageLabel.setText(messageBody)
        self.pokemonRecieved = messageBody
        if self.pokemonRecieved != ""  {
            setPokemonImage(name: self.pokemonRecieved)
            self.nameLabel.setText("Enter a name")
        }
    }
    


    
    // MARK: WatchKit Interface Controller Functions
    // ----------------------------------
    override func awake(withContext context: Any?) {
        super.awake(withContext: context)
        
        
        // 1. Check if teh watch supports sessions
        if WCSession.isSupported() {
            WCSession.default.delegate = self
            WCSession.default.activate()
        }
        
        
    }
    
    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()
        
        
    }
    
    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
    }

    
    // MARK: Actions
    // ---------------------
    
    // 2. When person presses button on watch, send a message to the phone
    @IBAction func buttonPressed() {
        
        if WCSession.default.isReachable {
            print("Attempting to send message to phone")
            self.messageLabel.setText("Sending msg to watch")
            WCSession.default.sendMessage(
                ["name" : "Pritesh"],
                replyHandler: {
                    (_ replyMessage: [String: Any]) in
                    // @TODO: Put some stuff in here to handle any responses from the PHONE
                    print("Message sent, put something here if u are expecting a reply from the phone")
                    self.messageLabel.setText("Got reply from phone")
            }, errorHandler: { (error) in
                //@TODO: What do if you get an error
                print("Error while sending message: \(error)")
                self.messageLabel.setText("Error sending message")
            })
        }
        else {
            print("Phone is not reachable")
            self.messageLabel.setText("Cannot reach phone")
        }
    }
    
    func setPokemonImage(name: String) {
        if name == "pikachu"{
            pokemonImageView.setImage(UIImage.init(named: "pikachu"))
        }
        else if name == "caterpie"{
            pokemonImageView.setImage(UIImage.init(named: "caterpie"))
        }else {
            pokemonImageView.setImage(UIImage.init(named: "pokeball"))
        }
    }
    
    
    // MARK: Functions for Pokemon Parenting
    @IBAction func nameButtonPressed() {
        print("name button pressed")
        let suggestedResponses = ["Pikachu", "Caterpie"]
        presentTextInputController(withSuggestions: suggestedResponses, allowedInputMode: .plain) {
            
            (results) in
            
            if (results != nil && results!.count > 0) {
                // 2. write your code to process the person's response
                let userResponse = results?.first as? String
                self.nameLabel.setText(userResponse)
                
            }
        }
        
    }

    @IBAction func startButtonPressed() {
        print("Start button pressed")
        self.startTimer()
    }
    
    @IBAction func feedButtonPressed() {
        self.hungerlevel  = self.hungerlevel - 12
        if self.hungerlevel < 0 {
            self.hungerlevel = 0
        }
        self.outputLabel.setText("Hunger: \(hungerlevel)  HP: \(healthlevel)")
        print("Feed button pressed")
    }
    
    @IBAction func hibernateButtonPressed() {
        print("Hibernate button pressed")
    }
    func startTimer() {
        timer = Timer.scheduledTimer(timeInterval: 1, target: self,   selector: (#selector(InterfaceController.updateTimer)), userInfo: nil, repeats: true)
    }
    
    
    @objc func updateTimer() {
        self.seconds = self.seconds + 1
        if seconds % 5 == 0 {
            self.hungerlevel = self.hungerlevel + 10
            self.outputLabel.setText("Hunger: \(hungerlevel)  HP: \(healthlevel)")
        }
        
        if self.hungerlevel > 20 {
            self.healthlevel = self.healthlevel - 5
            if self.healthlevel <= 0 {
                self.healthlevel = 0
                self.outputLabel.setText("Hunger: \(hungerlevel)  HP: \(healthlevel)")
                self.nameLabel.setText("\(self.pokemonName) died")
                timer.invalidate()
            }
            self.outputLabel.setText("Hunger: \(hungerlevel)  HP: \(healthlevel)")
        }
        
        //self.timelabel.setText("Time:\(seconds)")
    }
    
}
